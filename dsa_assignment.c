#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STUDENTS 100
#define MAX_NAME_LENGTH 51 // 50 characters + '\0'
#define DATE_LENGTH 11 // 10 characters (YYYY-MM-DD) + '\0'
#define REGISTRATION_LENGTH 7 // 6 digits + '\0'
#define PROGRAM_CODE_LENGTH 5 // 4 characters + '\0'

struct Student {
    char name[MAX_NAME_LENGTH];
    char dob[DATE_LENGTH];
    char registration[REGISTRATION_LENGTH];
    char program_code[PROGRAM_CODE_LENGTH];
    float tuition;
};

struct Student students[MAX_STUDENTS];
int num_students = 0;

// Function prototypes
void createStudent();
void readStudent();
void updateStudent();
void deleteStudent();
void searchStudent();
void sortStudentsByName();
void sortStudentsByRegistration();
void exportRecords();

int main() {
    int choice;

    do {
        printf("\nMenu:\n");
        printf("1. Create Student\n");
        printf("2. Read Student\n");
        printf("3. Update Student\n");
        printf("4. Delete Student\n");
        printf("5. Search Student\n");
        printf("6. Sort Students by Name\n");
        printf("7. Sort Students by Registration\n");
        printf("8. Export Records\n");
        printf("0. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch(choice) {
            case 1:
                createStudent();
                break;
            case 2:
                readStudent();
                break;
            case 3:
                updateStudent();
                break;
            case 4:
                deleteStudent();
                break;
            case 5:
                searchStudent();
                break;
            case 6:
                sortStudentsByName();
                break;
            case 7:
                sortStudentsByRegistration();
                break;
            case 8:
                exportRecords();
                break;
            case 0:
                printf("Exiting...\n");
                break;
            default:
                printf("Invalid choice! Please try again.\n");
        }
    } while(choice != 0);

    return 0;
}

void createStudent() {
    if (num_students >= MAX_STUDENTS) {
        printf("Maximum number of students reached!\n");
        return;
    }

    struct Student new_student;

    printf("Enter student name: ");
    scanf(" %[^\n]", new_student.name);

    printf("Enter student date of birth (YYYY-MM-DD): ");
    scanf("%s", new_student.dob);

    printf("Enter student registration number: ");
    scanf("%s", new_student.registration);

    printf("Enter student program code: ");
    scanf("%s", new_student.program_code);

    printf("Enter student annual tuition: ");
    scanf("%f", &new_student.tuition);

    students[num_students++] = new_student;
    printf("Student added successfully!\n");
}

void readStudent() {
    if (num_students == 0) {
        printf("No students found!\n");
        return;
    }

    printf("List of students:\n");
    for (int i = 0; i < num_students; i++) {
        printf("Student %d:\n", i + 1);
        printf("Name: %s\n", students[i].name);
        printf("Date of Birth: %s\n", students[i].dob);
        printf("Registration Number: %s\n", students[i].registration);
        printf("Program Code: %s\n", students[i].program_code);
        printf("Annual Tuition: %.2f\n", students[i].tuition);
    }
}

void updateStudent() {
    if (num_students == 0) {
        printf("No students found!\n");
        return;
    }

    char reg_num[REGISTRATION_LENGTH];
    printf("Enter student registration number to update: ");
    scanf("%s", reg_num);

    int found = 0;
    for (int i = 0; i < num_students; i++) {
        if (strcmp(students[i].registration, reg_num) == 0) {
            printf("Enter new student name: ");
            scanf(" %[^\n]", students[i].name);

            printf("Enter new student date of birth (YYYY-MM-DD): ");
            scanf("%s", students[i].dob);

            printf("Enter new student program code: ");
            scanf("%s", students[i].program_code);

            printf("Enter new student annual tuition: ");
            scanf("%f", &students[i].tuition);

            printf("Student updated successfully!\n");
            found = 1;
            break;
        }
    }

    if (!found) {
        printf("Student with registration number %s not found!\n", reg_num);
    }
}

void deleteStudent() {
    if (num_students == 0) {
        printf("No students found!\n");
        return;
    }

    char reg_num[REGISTRATION_LENGTH];
    printf("Enter student registration number to delete: ");
    scanf("%s", reg_num);

    int found = 0;
    for (int i = 0; i < num_students; i++) {
        if (strcmp(students[i].registration, reg_num) == 0) {
            for (int j = i; j < num_students - 1; j++) {
                students[j] = students[j + 1];
            }
            num_students--;
            printf("Student deleted successfully!\n");
            found = 1;
            break;
        }
    }

    if (!found) {
        printf("Student with registration number %s not found!\n", reg_num);
    }
}

void searchStudent() {
    if (num_students == 0) {
        printf("No students found!\n");
        return;
    }

    char reg_num[REGISTRATION_LENGTH];
    printf("Enter student registration number to search: ");
    scanf("%s", reg_num);

    int found = 0;
    for (int i = 0; i < num_students; i++) {
        if (strcmp(students[i].registration, reg_num) == 0) {
            printf("Student found:\n");
            printf("Name: %s\n", students[i].name);
            printf("Date of Birth: %s\n", students[i].dob);
            printf("Program Code: %s\n", students[i].program_code);
            printf("Annual Tuition: %.2f\n", students[i].tuition);
            found = 1;
            break;
        }
    }

    if (!found) {
        printf("Student with registration number %s not found!\n", reg_num);
    }
}

void sortStudentsByName() {
    if (num_students == 0) {
        printf("No students found!\n");
        return;
    }

    // Bubble sort implementation for simplicity
    for (int i = 0; i < num_students - 1; i++) {
        for (int j = 0; j < num_students - i - 1; j++) {
            if (strcmp(students[j].name, students[j + 1].name) > 0) {
                struct Student temp = students[j];
                students[j] = students[j + 1];
                students[j + 1] = temp;
            }
        }
    }

    printf("Students sorted by name:\n");
    readStudent(); // Print the sorted list
}

void sortStudentsByRegistration() {
    if (num_students == 0) {
        printf("No students found!\n");
        return;
    }

    // Bubble sort implementation for simplicity
    for (int i = 0; i < num_students - 1; i++) {
        for (int j = 0; j < num_students - i - 1; j++) {
            if (strcmp(students[j].registration, students[j + 1].registration) > 0) {
                struct Student temp = students[j];
                students[j] = students[j + 1];
                students[j + 1] = temp;
            }
        }
    }

    printf("Students sorted by registration number:\n");
    readStudent(); // Print the sorted list
}

void exportRecords() {
    FILE *fp;
    fp = fopen("student_records.csv", "w");

    if (fp == NULL) {
        printf("Error opening file!\n");
        return;
    }

    for (int i = 0; i < num_students; i++) {
        fprintf(fp, "%s,%s,%s,%s,%.2f\n", students[i].name, students[i].dob, students[i].registration, students[i].program_code, students[i].tuition);
    }

    fclose(fp);
    printf("Student records exported successfully!\n");
}
